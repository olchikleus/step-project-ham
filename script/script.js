"use strict"

// section our services
function toggleMenuForSectionOurServices(){
    document.querySelector(".our_services-list-items").addEventListener("click", (event) =>{
        if (event.target.tagName === "LI"){
            document.querySelectorAll(".our_services-list-items li.active").forEach((element) =>{
                element.classList.remove("active");
            });
            event.target.classList.add("active");

            document.querySelectorAll(".our_services-list-items-content li.active").forEach((elem) =>{
                elem.classList.remove("active")
            });

            const id = event.target.getAttribute ("data-name")
            document.querySelector(id).classList.add("active")

        }
    })
}
toggleMenuForSectionOurServices()


// section amazing work
function toggleMenuForSectionOurAmazingWork (){
    document.querySelectorAll(".our_amazing_work-list li").forEach((element)=>{
        element.addEventListener("click", (event) =>{
            event.target.closest("ul").querySelector(".active").classList.remove("active")
            event.target.classList.add("active")

            let btn0fSectionOurAmazingWork = document.querySelector(".our_amazing_work-btn")
            const attr = event.target.getAttribute ("data-filter")
            const list = document.querySelectorAll('.our_amazing_work-photo-list')
                list.forEach((element) => {
                    console.log(element)
                    element.classList.add("remove")
                    element.classList.remove("active")
                    if ((attr === element.dataset.id)) {
                        element.classList.remove("remove")
                        element.classList.add("active")
                        btn0fSectionOurAmazingWork.style.display = "none"
                    } else if (attr === "all") {
                        element.classList.remove("remove")
                        element.classList.add("active")
                        btn0fSectionOurAmazingWork.style.display = "flex"
                    }
                })
        })

    })
}
toggleMenuForSectionOurAmazingWork ()


// our_amazing_work (downland photo)
function loadMoreBtn (){

    let btn0fSectionOurAmazingWork = document.querySelector(".our_amazing_work-btn")
    let step = 12
    let allItems = document.querySelectorAll('.loadMore').length

    btn0fSectionOurAmazingWork.onclick = (event) => {
        event.target.classList.add('our_amazing_work-btn-loading')

        setTimeout(() => {
            event.target.classList.remove('our_amazing_work-btn-loading')
        }, 2000);

        let boxes = document.querySelectorAll('.loadMore')
        let items = boxes.length >= step ? step : boxes.length
        for (let i = 0; i < items; i++) {
                boxes[i].classList.remove("loadMore")
        }
        allItems -= step
        if (allItems <= 0) {
            btn0fSectionOurAmazingWork.style.display = "none"
        }
    }
}
loadMoreBtn ()



//feedbacks

const persons = [{
        name: 'Anna',
        comment: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        src: 'img/section_feedback/person1.png',
        id: 'anna',
        profession: 'Web designer',
    }, {
    name: 'Mike',
    comment: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium est fugiat molestiae natus pariatur perspiciatis provident saepe veniam veritatis vitae.',
    src: 'img/section_feedback/person2.png',
    id: 'mike',
    profession: 'Javascript developer',
},{
    name: 'Jack',
    comment: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    src: 'img/section_feedback/person3.png',
    id: 'jack',
    profession: 'Graphic Designer',
},{
    name: 'Adriana',
    comment: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium est fugiat molestiae natus pariatur perspiciatis provident saepe veniam veritatis vitae.',
    src: 'img/section_feedback/person4.png',
    id: 'adriana',
    profession: 'UX Designer',
}]

function selectPerson(index) {

    let comment = document.getElementById("comment");
    comment.innerHTML = persons[index].comment;

    let name = document.getElementById("name");
    name.innerHTML = persons[index].name;

    let profession = document.getElementById("profession");
    profession.innerHTML = persons[index].profession;

    let avatar = document.getElementById("avatar");
    avatar.src = persons[index].src;
}

let activePerson = 0
selectPerson(activePerson)
let personsImages

function togglePersonForSectionFeedbacks(){
    personsImages = document.querySelectorAll(".feedbacks-persons-list li img")
    personsImages.forEach((element, index)=>{
        element.addEventListener('click',(event)=>{
            personsImages.forEach((elem)=>{
                elem.classList.remove("active")
            })
            event.target.classList.add("active")
            activePerson = index
        })
    })
}
togglePersonForSectionFeedbacks()

function activatedImage(){
    personsImages.forEach((elem)=>{
        elem.classList.remove("active")
    })
    document.getElementById(persons[activePerson].id).classList.add("active")
}

function next () {
    activePerson++
    if(activePerson >= personsImages.length){
        activePerson = 0
    }
    selectPerson(activePerson)
    activatedImage()
}

function previous () {
    if(activePerson < 1){
        activePerson = personsImages.length
    }
    activePerson--
    selectPerson(activePerson)
    activatedImage()
}







